<?php $this->load->view('includes/header');?>

<body>
<h1 class="ui header">Comparing Numbers</h1>
<div class="ui container">
  <div class="ui segments">
    <div class="ui center aligned segment">
    <h3><strong><?php
		// Printing an array using inputNum function in TwoNumbers class.
		if (isset($num)){
		    foreach ($num as $key) {
		    if ($key->larger != $key->smaller) {
		    echo "Larger Number: ".$key->larger."<br>";
		    echo "Smaller Number: ".$key->smaller."<br>";
		    }else{
		    echo "You entered the same number! ".$key->larger.' = '.$key->smaller;
		    }
		   }
		}elseif (isset($str)) {
		echo '<font color="red">'.$str.'</font>';
		}
	?></strong></h3>
    </div>
  </div>
</div>
</body>
</html>