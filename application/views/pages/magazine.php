<?php $this->load->view('includes/header');?>

<body>
<div class="ui grid container">
  <div class="row">
    <div class="column">
      <h1 class="ui header">Magazine Page</h1>
    </div>
  </div>
   <div class="row">
    <div class="column center aligned ">
      <div class="ui message">
	        <div class="ui list">
		        <div class="item">
			        <?php
						// Printing "My Magazine!" string using a loop of header tags.
						for ($i=1; $i <= 6 ; $i++) { 
							echo $data = '<h'.$i.'>'. "My Magazine!".'</h'.$i.'>';
						}
					?>
				</div>
			</div>
		<div class="column left aligned">
        <p>The string above is on a loop of header tags from h1 to h6. This is our first activity in ICT 141.</p>
        <a onclick="window.history.back()" class="ui blue button">&laquo; Back </a>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>

