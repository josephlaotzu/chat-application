<?php $this->load->view('includes/header');?>

<body>
<?php if (isset($date)) { ?>
<h1 class="ui header">Current Date</h1>
<div class="ui container">
  <div class="ui segments">
    <div class="ui center aligned segment">
    <h3><?= $date; ?></h3>
    </div>
  </div>
</div>
<?php exit(); }
if (isset($_POST['save'])) { ?>
<h1 class="ui header">New Magazine Info</h1>
<div class="ui container">
  <div class="ui segments">
    <div class="ui center aligned segment">
    <h4>Title Name: </h4><?= $_POST['title'];?><br>
    <h4>Title Description: </h4><?php
	if (empty($_POST['description'])) {
		echo '<font color="red">This magazine has no description.</font>';
	}else{
		echo $_POST['description'];
	}
    ?><br>
    <h4>New Edition: </h4><?= $_POST['new-edition'];?><br>
    <h4>Published Date: </h4><?= $_POST['month'].' '.$_POST['day'].', '.$_POST['year'];?>
    </div>
  </div>
</div>
<?php exit(); } ?>

<h1 class="ui header">Add Magazine Title</h1>
<div class="ui container">
	<div class="row">
		<div class="column">
			<form action="" method="POST">
				<div class="ui form">
				  <div class="three fields">
				    <div class="field"><br>
				      	<label>Title name</label>
				      	<input type="text" name="title" placeholder="Title Name" required autofocus>
						<br><br>
					 	<label>Title description</label>
					 	<textarea rows="2" name="description" value="text"></textarea>
						<br><br>
				   		<label>Is this a new edition?</label>
				        <input type="radio" name="new-edition" checked="checked" value="yes">Yes
				        <input type="radio" name="new-edition" value="no">No
						<br><br>

						<label>Publish Date:</label>
						<div class="ui grid container">
						<div class="six wide column">
					    <label>Month</label>
					    <?php $month = array('January','Febuary','March','April','May','June','July','August','September','October','November','December',);?>
					    <select name="month">
					    <?php foreach ($month as $m) {
					    	echo "<option value=".$m.">$m</option>";
					    }

					    ?>
					    </select></div>
						<div class="four wide column">
					    <label>Day</label>
					    <select name="day">
						<?php for ($i=1; $i <=31; $i++) { 
						echo "<option value=".$i.">$i</option>";
						}
						?>
					    </select></div>
						<div class="five wide column">
					    <label>Year</label>
					   	<select name="year">
						<?php for ($i=1990; $i <=2015; $i++) { 
						echo "<option value=".$i.">$i</option>";
						}
						?>
					    </select></div>
					    </div><br><br>
					    <button  type="submit" name="save" value="save" class="ui primary button">Submit</button>
					  </div>
				  </div>
				</div>
			</form>
		</div>
	</div>
</div>
</body>
</html>