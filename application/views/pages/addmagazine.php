<?php $this->load->view('includes/header');?>

<body>
<h1 class="ui header">Add Magazine</h1>
<div class="ui container">
  <div class="ui segments">
    <div class="ui center aligned segment">
    <p><?php
		// Printing a string using addMagazine function in Magazine class.
		if (isset($item)){
		    foreach ($item as $key) {
		    echo 'You have new magazine named '.'<b>'.$key->title.'</b>'.' published in the year '. '<b>'.$key->year.'</b>'.' by '.'<b>'.$key->name.'</b>'.'.';
		    }
		}
	?></p>
    </div>
  </div>
</div>
</body>
</html>

