<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Title extends CI_Controller {

	public function _remap($uri){
		if ($uri == "addtitle") {
			$this->index();
		}else{
			$this->_showDate();
		}

	}

	public function index()
	{	
		$this->load->view('pages/addtitle');
	}

	private function _showDate(){
		$var['date'] = date("F d, Y");
		$this->load->view('pages/addtitle',$var);
	}
	
}
