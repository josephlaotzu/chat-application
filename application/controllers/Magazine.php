<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Magazine extends CI_Controller {

	public function index()
	{	
		$this->load->view('pages/magazine');
	}

	public function addMagazine($var1, $var2, $var3)
	{
		$data['item'][] = (object) array('title' => $var1 , 'year' => $var2 , 'name' => $var3);
		$this->load->view('pages/addmagazine', $data);
	}

	
}
