<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TwoNumbers extends CI_Controller {

	public function index()
	{	
		$this->load->view('pages/twonumbers_view');
	}

	public function inputNum($var1,$var2){
		if (is_numeric($var1) && is_numeric($var2)) {
		if ($var1 > $var2) {
		$data['num'][] = (object) array('larger' => $var1 , 'smaller' => $var2);
		}elseif ($var1 < $var2) {
		$data['num'][] = (object) array('larger' => $var2 , 'smaller' => $var1);
		}else{
		$data['num'][] = (object) array('larger' => $var1 , 'smaller' => $var2);
		}
		$this->load->view('pages/twonumbers_view',$data);
		}else{
		$data['str'] = "Please enter numeric values!";
		$this->load->view('pages/twonumbers_view',$data);
		}
	}
	
}
